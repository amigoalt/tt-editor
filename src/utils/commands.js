// import {Selection, TextSelection, NodeSelection, AllSelection} from "prosemirror-state"
import { findParentNode, hasParentNodeOfType } from 'prosemirror-utils'

function newClass(toggledClassName, target)	{
	let currentClasses = []
	currentClasses = target.node.attrs.classNames
	currentClasses = currentClasses.replace(/\s+/g,' ').trim()
	currentClasses = currentClasses.split(' ');
	if (currentClasses.length == 1 && currentClasses[0] === "") currentClasses = []

	// toggle on/off the class name
	let classNameFound = false
	let newClasses = function (accumulator, currentValue,) {
		if (currentValue === toggledClassName) {
			classNameFound = true
		} else {
			accumulator.push(currentValue)
		}
		return accumulator
	}
	currentClasses = currentClasses.reduce(newClasses, [])

	// add the classname if not found (toggle on class)
	currentClasses = classNameFound ? currentClasses : currentClasses.concat([toggledClassName])

	return currentClasses.join(" ")
}

export function toggleClass(allowedPlugins, schema, className) {
	return function(state, dispatch) {
		// let {from, to} = state.selection
		let predicate = () => false
		let target = null
		let nodeType = null
		loop:
		for(let type in allowedPlugins)	{
			nodeType = schema.nodes[type]
			if (hasParentNodeOfType(nodeType)(state.tr.selection)) {
				predicate = node => node.type === nodeType;
				target = findParentNode(predicate)(state.tr.selection);
				break loop
			}
		}

		let newClasses = newClass(className, target)
		let attrs = { ...target.node.attrs }
		attrs.classNames = newClasses
		if (dispatch) {
			dispatch(state.tr.setNodeMarkup(target.pos, nodeType, attrs))
		}
		return true
	}
}