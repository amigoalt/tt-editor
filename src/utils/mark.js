import {Plugin, PluginKey} from "prosemirror-state"
// import { toggleMark } from 'prosemirror-commands'
// import {schema} from "prosemirror-schema-basic"


export default class Mark {
	constructor()	{
		this.editorView = "not initialized yet.."
		this.editorSchema = "not initialized yet.."
		this.editorState = "not initialized yet.."
	}
	  get plugin() {
	  	console.log(this)
	  	return new Plugin({
	  		key: new PluginKey(this.name),
	  		props: {
	  			handleKeyDown: (editorView, event) => {
	  				this.editorView = editorView
	  				if(event.ctrlKey && event.code === this.key) {
	  					let command = this.command()
	  					this.runCommand(command)
	  				}
	  			}
	  		},
		    view: (editorView) => {
		    	// set it here for on:click possibility
		    	this.editorView = editorView
		    	return {
		    		update: (editorView, eState) => {
		    			// console.log(this.name + " plugin UPDATE has bee called!")
		      		},
		      		destroy: () => {
		      			// console.log("Bold plugin DESTROY has bee called!")
		    		}
		    	}
		    },
	  	})
	  }
	  
	  runCommand(attrs={}) {
	  	return () => {
	  		this.command(attrs)(this.editorView.state, this.editorView.dispatch, this.editorView)
	  	}
	  }

	  inputRule() {
    	return null
	  }
}